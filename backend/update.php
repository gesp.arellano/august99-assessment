<?php require_once "class.php";

if(!empty($_POST)){
	$catalog = new catalog;
	$catalog->update(
        $_POST['title'],
        $_POST['isbn'],
        $_POST['author'],
        $_POST['publisher'],
        $_POST['year_published'],
        $_POST['category'],
        $_POST['id'],
    );
}
?>