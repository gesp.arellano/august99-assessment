(function () {
    
    var modal_catalog = $('#modal-catalog');
    var btn_modal_catalog = $('#btn-modal-catalog');
    var table_catalog = $('#table-catalog');
    var table_catalog_body = table_catalog.find('tbody');


    var txt_title = modal_catalog.find('#txt-title');
    var txt_isbn = modal_catalog.find('#txt-isbn');
    var txt_author = modal_catalog.find('#txt-author');
    var txt_publisher = modal_catalog.find('#txt-publisher');
    var txt_year_published = modal_catalog.find('#txt-year-published');
    var txt_category = modal_catalog.find('#txt-category');

    modal_catalog.find('#btn-add-catalog').on('click', addCatalog);
    modal_catalog.find('#btn-save-changes').on('click', saveChanges);

    btn_modal_catalog.on('click', resetBtns);

    table_catalog.delegate(".btn-edit", "click", updateCatalog);
    table_catalog.delegate(".btn-delete", "click", deleteCatalog);
    
    
    init();

    function init() {
        let catalog  = getValues();
        $.post("backend/read.php",function(data, status){
            let rows = JSON.parse(data);
            let table_body = "";

      
            rows.forEach(row => {
                
                table_body += "<tr>";
                table_body += "<td scope='col'>";
                table_body += row.title;
                table_body += "</td>";

                table_body += "<td scope='col'>";
                table_body += row.isbn;
                table_body += "</td>";

                table_body += "<td scope='col'>";
                table_body += row.author;
                table_body += "</td>";

                table_body += "<td scope='col'>";
                table_body += row.publisher;
                table_body += "</td>";
                
                table_body += "<td scope='col'>";
                table_body += row.category;
                table_body += "</td>";

                table_body += "<td scope='col'>";
                table_body += "<a class='btn btn-custom btn-edit btn-sm ' href='#' role='button'  data-title="+row.title+" data-isbn="+row.isbn+"  data-year_published="+row.year_published+"  data-author="+row.author+" data-publisher="+row.publisher+" data-category="+row.category+" data-id="+row.id+">EDIT</a> &nbsp;"; 

               

                table_body += "<a class='btn btn-custom btn-delete btn-sm ' href='#' role='button' data-id='"+row.id+"'>DEL</a>";
                table_body += "</td>";
                table_body += "</tr>";
            });
           
            table_catalog_body.html(table_body);


        });
       
    }
    function resetBtns(){
        modal_catalog.find('#btn-add-catalog').removeClass('d-none');
        modal_catalog.find('#btn-save-changes').addClass('d-none');
    }


    function addCatalog() {
        let catalog  = getValues();
        if (!catalog) {
            alert("Please suffice all fields");
            return false;
        }
        $.post("backend/create.php", catalog,
        function(data, status){
            alert(data)
            modal_catalog.find('.form-control').val("");
            modal_catalog.modal('toggle');
            init();

        });
    }
    function saveChanges() {
        let catalog  = getValues();
        if (!catalog) {
            alert("Please suffice all fields");
            return false;
        }
        catalog['id'] = $(this).data('id');
        $.post("backend/update.php", catalog,
        function(data, status){
            alert(data)
            modal_catalog.find('.form-control').val("");
            modal_catalog.modal('toggle');
            init();

        });
        modal_catalog.find('.form-control').val("");
        
    }


    function updateCatalog(){
        btn_modal_catalog.trigger('click')
        modal_catalog.find('#btn-add-catalog').addClass('d-none');
        modal_catalog.find('#btn-save-changes').removeClass('d-none');


        txt_title.val($(this).data('title'));
        txt_isbn.val($(this).data('isbn'));
        txt_author.val($(this).data('author'));
        txt_publisher.val($(this).data('publisher'));
        txt_year_published.val($(this).data('year_published'));
        txt_category.val($(this).data('category'));
        modal_catalog.find('#btn-save-changes').attr("data-id", $(this).data('id'))
    }

    function deleteCatalog(){

        let catalog = {
            id: $(this).data('id')
        }
        $.post("backend/delete.php", catalog,
        function(data, status){
            alert(data);
            init();
        });
    }

    function getValues() {

        if (txt_title.val() && txt_isbn.val() && txt_author.val() && txt_publisher.val() && txt_year_published.val() && txt_category.val()) {
            return {
                title: txt_title.val(),
                isbn: txt_isbn.val(),
                author: txt_author.val(),
                publisher: txt_publisher.val(),
                year_published: txt_year_published.val(),
                category: txt_category.val(),
            }
        }
        else{
           
            return false;
        }
       
    }
})();