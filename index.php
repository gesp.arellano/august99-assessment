<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/app.css"> 
    <title>August 99 - Web Developer Assessment</title>

</head>
<body>
<div class="container wrapper">

    <div class="add-wrapper">
        <button type="button" id="btn-modal-catalog" class="btn btn-success btn-add mb-4" data-bs-toggle="modal" data-bs-target="#modal-catalog">Add</button>
    </div>
    
    <table id="table-catalog" class="table table-hover ">
        <thead>
            <tr>
                <th scope="col">TITLE</th>
                <th scope="col">ISBN</th>
                <th scope="col">AUTHOR</th>
                <th scope="col">PUBLISHER</th>
                <th scope="col">CATEGORY</th>
                <th scope="col">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="modal-catalog" tabindex="-1" aria-labelledby="modal-catalog-label" aria-hidden="true">
    <form>
    <div class="modal-dialog">
        <div class="modal-content">
       
        <div class="modal-header">
            <h5 class="modal-title" id="modal-catalog-label">Book Catalog</h5>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="alert alert-primary d-none" role="alert"></div>
            <div class="mb-3">
                <label for="txt-title" class="form-label">Title</label>
                <input type="text" class="form-control" required id="txt-title">
            </div>
            <div class="mb-3">
                <label for="txt-isbn" class="form-label">ISBN</label>
                <input type="text" class="form-control" required id="txt-isbn">
            </div>
            <div class="mb-3">
                <label for="txt-author" class="form-label">Author</label>
                <input type="text" class="form-control" required id="txt-author">
            </div>
            <div class="mb-3">
                <label for="txt-publisher" class="form-label">Publisher</label>
                <input type="text" class="form-control" required id="txt-publisher">
            </div>
            <div class="mb-3">
                <label for="txt-year-published" class="form-label">Year Published</label>
                <input type="number" placeholder="YYYY" min="1600" max="2022" class="form-control" required id="txt-year-published">
            </div>
            <div class="mb-3">
                <label for="txt-category" class="form-label">Category</label>
                <input type="text" class="form-control" required id="txt-category">
            </div>
        
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" id="btn-add-catalog" class="btn btn-primary">Add</button>
            <button type="submit" id="btn-save-changes" class="btn btn-warning d-none">Save Changes</button>
        </div>
        </div>
    </div>
    </div>
    </form>
    </div>

<script src="https://code.jquery.com/jquery-3.6.2.min.js" integrity="sha256-2krYZKh//PcchRtd+H+VyyQoZ/e3EcrkxhM8ycwASPA=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script src="assets/app.js"></script>
</body>
</html>