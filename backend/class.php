<?php 
require_once "env.php"; 
class catalog extends db {

    public function view()
    {
        $query = "SELECT * FROM catalogs";
		$stmt = $this->connect()->prepare($query);
		
		$stmt->execute();

		echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));

		
	
    }
    public function insert($title,$isbn,$author,$publisher,$year_published,$category)
    {
        $query = "INSERT INTO catalogs(title,isbn,author,publisher,year_published,category) VALUES(?,?,?,?,?,?) ";
		$stmt = $this->connect()->prepare($query);
		if($stmt->execute([$title,$isbn,$author,$publisher,$year_published,$category])){
			echo "Catalog has been added!";
		}
    }

    public function update( $title,$isbn,$author,$publisher,$year_published,$category, $id)
    {
        $query = "UPDATE catalogs SET title=?, isbn=?, author=?, publisher=?, year_published=?, category=? where id = ?";
		$stmt = $this->connect()->prepare($query);
		if($stmt->execute([$title,$isbn,$author,$publisher,$year_published,$category, $id])){
			echo "Catalog has been updated!";
		}
    }

    public function delete($id){
		$query = "DELETE FROM catalogs WHERE id = ?";
		$stmt = $this->connect()->prepare($query);
		if($stmt->execute([$id])){
			echo "Catalog has beed deleted";
		}
	}
}

?>